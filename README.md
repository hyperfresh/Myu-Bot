![](https://cdn.iwa.sh/img/q-bot_logo.png)

**This isn't the official Q-Bot repository! This is a fork of it.
I created the fork to contribute to the real repository.
If you want to see the Q-Bot repository, go to
https://gitlab.com/iwaQwQ/Q-Bot**

// note to iwa: delete above when merging

## About

**Q-Bot is entirely developed and handled by iwa**

- Language used : `JavaScript, with NodeJS`
- Library used : `discord.js`
